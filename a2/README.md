# LIS4381 - Mobile Web Application Development

## Gal Wolf

### Assignment 2 Requirements:

*Three Parts:*

1. Create a mobile recipe app using Android Studio.
2. Create a README.md file
3. Chapter Questions (Chs 3,4)

#### README.md file should include the following items:

* Course title, your name, assignment requirements;
* Screenshot of running applications's first user interface;
* Screenshot of running application's second user interface


#### Assignment Screenshots:

*Screenshot of Arunning applications's first user interface*:

![First User Interface Screenshot](img/first.png)

*Screenshot of running application's second user interface*:

![Second User Interface Screenshot](img/second.png)


#### Bitbucket Link:

*a) Assignment:*
[A2 Link](https://bitbucket.org/gw17c/lis4381/src/master/a2/)
