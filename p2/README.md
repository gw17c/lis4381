# LIS4381 - Mobile Web Application Development

## Gal Wolf

### Project 2 Requirements:

*Four Parts:*

1. Requires A4 cloned files.
2. Review subdirectories and files
3. Open index.php and review code:
4. Answer chapter questions

#### README.md file should include the following items:

* Course title, your name, assignment requirements;
* Screenshot as per examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381/;

#### Assignment Screenshots:

*Screenshot of Project 2 (index.php)*:

![Index Page](img/index.png) 

*Screenshot of edit_petstore.php*:

![Add Petstore](img/editpetstore.png)

*Screenshot of edit_petstore_process.php*:

![Error Page](img/editpetstoreprocess.png)

*Screenshot of Carousel*:

![Error Page](img/main.png)

*Screenshot of RSS Feed*:

![Error Page](img/rssfeed.png)

#### Links:

*a) Assignment:*
[P2 Link](https://bitbucket.org/gw17c/lis4381/src/master/p2/)

*b) Local LIS4381 Web App:*
[Localhost Link](http://localhost/repos/lis4381/)



