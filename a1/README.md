> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Gal Wolf

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)

#### README.md file should include the following items:

* Screenshot of AAMPPS Installation [My PHP Installation](http://localhost/cgi-bin/phpinfo.cgi);
* Screenshot of running java Hello;
* Screenshot of running Android Studio- My First App;
* git commands w/short descriptions;
* Bitbucket repo links: a) the assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create an empty git repo
2. git status - show the working tree status
3. git add - add file contents to the index
4. git commit - record changes to the repo
5. git push - update remote refs along w/ associated objects
6. git pull - fetch from and integrate with another repo
7. git config - get and set repo or global options

#### Assignment Screenshots:

*Screenshot of AMPPS running [My PHP Installation](http://localhost/cgi-bin/phpinfo.cgi)*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Bitbucket Repo Links:

*a) Assignment:*
[A1 Link](https://bitbucket.org/gw17c/lis4381/src/master/a1/)

*b) BitbucketStationLocations tutorial:*
[Tutorial Repo Link](https://bitbucket.org/gw17c/bitbucketstationlocations/src/master/)
