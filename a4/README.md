# LIS4381 - Mobile Web Application Development

## Gal Wolf

### Assignment 4 Requirements:

*Three Parts:*

1. cd to local repos subdirectory
2. Clone assignment starter files
3. Review subdirectories and files
4. *Must* delete .git subdirectory
5. Open index.php and review code:
    1. Suitably modify meta tags
    2. Changetitle,navigationlinks,andheadertagsappropriately
    3. Add form controls to match attributes of petstore entity
    4. Add the following jQuery validation and regular expressions-- as per the entity
attribute requirements (and screenshots below):
        1. *All* input fields, except Notes are required
        2. Use min/max jQuery validation
        3. Use regexp to only allow appropriate characters for each control: pst_name, pst_street, pst_city, pst_state, pst_zip, pst_phone, pst_email, pst_url, pst_ytd_sales, pst_notes
        Name (provided): letters, numbers, hyphens, and underscore
        Street, City (provided):
        no more than 30 characters
        Street: must only contain letters, numbers, commas, hyphens, or periods
        City: can only contain letters, numbers, hyphens, and space character (29 Palms) State:
        must be 2 characters
        must only contain letters
        Zip:
        must be between 5 and 9 digits, inclusive
        must only contain numbers
        Phone:
        must be 10 digits, including area code
        must only contain numbers
        Email: See http://www.qcitr.com/usefullinks.htm#lesson7
        URL:
        no more than 100 characters
        See http://www.qcitr.com/usefullinks.htm#lesson7
        YTD Sales:
        no more than 10 digits, including decimal point
        can only contain numbers, and decimal point (if used)
        4. *After* testing jQuery validation, use HTML5 property to limit the number of characters for each control
        5. Research what the following validation code does:
        valid: 'fa fa-check',
        invalid: 'fa fa-times',
        validating: 'fa fa-refresh'
6. Create a favicon using *your* initials, and place it in each assignment’s main directory,including lis4381: Example: https://www.favicon-generator.org/
7. Answer chapter questions


#### README.md file should include the following items:

* Course title, your name, assignment requirements;
* Screenshot as per examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381/;

#### Assignment Screenshots:

*Screenshot of LIS4381 Portal (main page)*:

![Main Page](img/main.png) 

*Screenshot of Failed Validation*:

![Failed Validation](img/valid.png)

*Screenshot of Passed Validation*:

![Failed Validation](img/invalid.png)

#### Links:

*a) Assignment:*
[A4 Link](https://bitbucket.org/gw17c/lis4381/src/master/a4/)

*b) Local LIS4381 Web App:*
[Localhost Link](http://localhost/repos/lis4381/)



