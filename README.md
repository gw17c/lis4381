> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>

# LIS4381 - Mobile Web Application Development

## Gal Wolf

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/gw17c/lis4381/src/master/a1/README.md)
    * Install AAMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/gw17c/lis4381/src/master/a2/README.md)
    * Create Healthy Recipes Android app
    * Provide Screenshots of completed app
3. [A3 README.md](https://bitbucket.org/gw17c/lis4381/src/master/a3/README.md)
    * Create ERD based upon business rules
    * Provide screenshot of completed ERD
    * Provide DB resource links
4. [A4 README.md](https://bitbucket.org/gw17c/lis4381/src/master/a4/README.md)
    * cd to local repos subdirectory
    * Clone assignment starter files
    * Review subdirectories and files
    * *Must* delete .git subdirectory
    * Open index.php and review code:
    * Create a favicon using *your* initials, and place it in each assignment’s main directory,including lis4381: Example: https://www.favicon-generator.org/
    * Answer chapter questions
5. [A5 README.md](https://bitbucket.org/gw17c/lis4381/src/master/a5/README.md)
    * Clone A4 files
    * Review subdirectorites and files
    * Open index.php and review code
    * Turn off client-side validation
    * Answer chapter questions
6. [P1 README.md](https://bitbucket.org/gw17c/lis4381/src/master/p1/README.md)
    * Create My Business Card Android app
    * Provide screenshots of completely app
7. [P2 README.md](https://bitbucket.org/gw17c/lis4381/src/master/p2/README.md)
    * Clone A4 files
    * Review subdirectorites and files
    * Open index.php and review code
    * Answer chapter questions
