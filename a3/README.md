# LIS4381 - Mobile Web Application Development

## Gal Wolf

### Assignment 3 Requirements:

*Three Parts:*

1. Create database in MySQL Worbench.
2. Create mobile app in Android Studio.
2. Create a README.md file
3. Chapter Questions 

#### README.md file should include the following items:

* Course title, your name, assignment requirements;
* Screenshot of ERD;
* Screenshot of running applications's first user interface;
* Screenshot of running application's second user interface;
* Links to the following files:
    a. a3.mwb
    b. a3.sql


#### Assignment Screenshots:

*Screenshot of ERD*:
![ERD screenshot](img/a3.png)

*Screenshot of running applications's first user interface*:

![First User Interface Screenshot](img/rsz_first.png)

*Screenshot of running application's second user interface*:

![Second User Interface Screenshot](img/rsz_second.png)


#### Links:

*a) Assignment:*
[A3 Link](https://bitbucket.org/gw17c/lis4381/src/master/a3/)

*b) a3.mwb:*
[a3.mwb](https://bitbucket.org/gw17c/lis4381/src/master/a3/a3.mwb)

*c) a3.sql:*
[a3.sql](https://bitbucket.org/gw17c/lis4381/src/master/a3/a3.sql)

