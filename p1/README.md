# LIS4381 - Mobile Web Application Development

## Gal Wolf

### Project 1 Requirements:

*Three Parts:*

1. Create mobile app in Android Studio.
2. Create a README.md file
3. Chapter Questions 

#### README.md file should include the following items:

* Course title, your name, assignment requirements;
* Screenshot of running applications's first user interface;
* Screenshot of running application's second user interface;

#### Assignment Screenshots:

*Screenshot of running applications's first user interface and second user interface*:

![First User Interface Screenshot](img/first.png) ![Second User Interface Screenshot](img/second.png)

#### Links:

*a) Assignment:*
[P1 Link](https://bitbucket.org/gw17c/lis4381/src/master/p1/)



